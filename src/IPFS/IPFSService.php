<?php

namespace Cowaboo\Models\IPFS;

use API;
use Storage;

/**
 *
 */
class IPFSService {
	// protected $baseUrl = "https://gateway.ipfs.io/ipfs/";
	/**
	 * @var string
	 */
	protected $baseUrl = "http://ipfs.stadja.net:8080/ipfs/";

	/**
	 * @var mixed
	 */
	protected $storage;

	public function __construct() {
		$this->storage = Storage::disk('ipfs');
	}

	/**
	 * @param  $data
	 * @return mixed
	 */
	public function create($data) {
		if (!is_string($data)) {
			$data = json_encode($data);
		}
		$post = array('data' => $data);
		$create = API::post('ipfs/raw', $post);
		$create = json_decode($create);
		if (!isset($create->Hash) || !$create->Hash) {
			dd($create, $data);
		}
		$this->save($create->Hash, $data);
		return $create->Hash;
	}

	/**
	 * @param  $ipfs
	 * @return mixed
	 */
	public function findType($ipfs) {
		if (!$ipfs) {
			return false;
		}

		if (is_string($ipfs)) {
			return 'string';
		}

		$ipfs = get_object_vars($ipfs);
		if (sizeof($ipfs) != 1) {
			return 'unknown';
		}

		$first_key = key($ipfs);

		return $first_key;
	}

	/**
	 * @param  $hash
	 * @return mixed
	 */
	public function get($hash) {
		$ipfs = $this->getRaw($hash);

		$ipfsObject = json_decode($ipfs);
		if ($ipfsObject) {
			$ipfs = $ipfsObject;
		}
		return $ipfs;

	}

	/**
	 * @param  $hash
	 * @param  $fromGateway
	 * @return mixed
	 */
	public function getRaw($hash, $fromGateway = false) {
		$url = $this->baseUrl . $hash;

		list($basePath, $fullPath) = $this->getPath($hash);

		if (!$this->storage->exists($fullPath) || !$this->storage->get($fullPath)) {
			$ipfs = $this->curl($url);
			if (!$ipfs) {
				$ipfs = $this->curl($url);
			}

			if ($fromGateway) {
				return $ipfs;
			}

			$this->save($hash, $ipfs);
		}

		$ipfs = $this->storage->get($fullPath);

		return $ipfs;
	}

	/**
	 * @param $hash
	 * @param $ipfs
	 */
	public function save($hash, $ipfs) {
		list($basePath, $fullPath) = $this->getPath($hash);
		$this->storage->makeDirectory($basePath);
		$this->storage->put($fullPath, $ipfs);

		return true;
	}

	/*
		    inspired by https://github.com/ipfspics/server/blob/master/app/class/ipfs.class.php
	*/
	/**
	 * @param  $url
	 * @param  $data
	 * @return mixed
	 */
	protected function curl($url, $data = "") {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 3);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);

		if ("" != $data) {
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: multipart/form-data; boundary=a831rwxi1a3gzaorw1w2z49dlsor'));
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, "--a831rwxi1a3gzaorw1w2z49dlsor\r\nContent-Type: application/octet-stream\r\nContent-Disposition: file; \r\n\r\n" . $data . "    a831rwxi1a3gzaorw1w2z49dlsor");
		}
		$output = curl_exec($ch);
		if (false == $output) {
			//todo: when ipfs doesn't answer
		}
		curl_close($ch);

		return $output;
	}

	/**
	 * @param $hash
	 */
	protected function getPath($hash) {
		$basePath = substr($hash, 0, 4) . '/' . substr($hash, 4, 2);
		$fullPath = $basePath . '/' . $hash;
		return array($basePath, $fullPath);
	}
}
