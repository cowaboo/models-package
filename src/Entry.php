<?php

namespace Cowaboo\Models;
use Cowaboo\Models\IPFSable;
use GrahamCampbell\Markdown\Facades\Markdown;

class Entry extends IPFSable {
	/**
	 * @var array
	 */
	protected $keys = array('tags', 'value', 'author', 'date', 'previous', 'conf');

	/**
	 * @var string
	 */
	protected $mainKey = 'entry';

	public function getValueHtmlAttribute() {
		return Markdown::convertToHtml($this->value);
	}

	/**
	 * @return mixed
	 */
	public function getRawTagsAttribute() {
		$tags = $this->tags;
		$tags = array_merge(array_filter(explode('||', $tags)));

		$rawTags = '';
		foreach ($tags as $key => $tag) {
			if ($key > 0) {
				$rawTags .= ', ';
			}
			$rawTags .= $tag;
		}

		return $rawTags;
	}

	/**
	 * @return mixed
	 */
	public function getTagsAttribute() {
		if (!isset($this->attributes['tags'])) {
			$this->attributes['tags'] = '';
		}

		if (!is_string($this->attributes['tags'])) {
			$this->attributes['tags'] = '||' . implode('||', $this->attributes['tags']) . '||';
		}
		return $this->attributes['tags'];
	}

	/**
	 * @param $tags
	 * @return mixed
	 */
	public function setTagsAttribute($tags) {
		$this->attributes['tags'] = $tags;
		if (!is_array($tags)) {
			$this->attributes['tags'] = array_filter(explode('||', $tags));
		}
		sort($this->attributes['tags']);
		return $this->attributes;
	}
}
