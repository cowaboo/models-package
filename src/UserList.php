<?php

namespace Cowaboo\Models;

use Storage;

class UserList extends IPFSable {
	protected $keys = array('list', 'previous', 'date');
	protected $mainKey = 'user_list';

	static function getCurrent() {
		$current = new self();
		if (Storage::disk('shared')->has('userList')) {
			$hash = Storage::disk('shared')->get('userList');
			$current = self::createFromHash($hash);
		}
		return $current;
	}

	public function addUser($email, $publicAddress) {
		$newList = $this->createNewVersion();
		$list = clone ($newList->list);
		$list->$email = $publicAddress;
		$newList->list = $list;

		if ($newList->list == $this->list) {
			return $this->hash;
		}
		return $newList->save();
	}

	public function userExists($email) {
		$user = $this->findUser($email);
		return $user ? true : false;
	}

	public function findUser($email) {
		$list = get_object_vars($this->list);
		return isset($list[$email]) ? $list[$email] : false;
	}

	public function findEmailByPublicAddress($publicAddress) {
		$list = get_object_vars($this->list);
		$email = array_search($publicAddress, $list);
		return $email;
	}

	public function get() {
		return $this;
	}

	// public function saveDictionary($dictionary)
	// {
	// 	$id = $dictionary->id;
	// 	$newUserList = $this->createNewVersion();
	// 	$list = clone($newUserList->list);
	// 	$list->$id = $dictionary->UserList;
	// 	$newUserList->list = $list;

	// 	if ($newUserList->list == $this->list) {
	// 		return $this->hash;
	// 	}
	// 	return $newUserList->save();
	// }

	public function getListAttribute() {
		if (!isset($this->attributes['list']) || !$this->attributes['list']) {
			$this->attributes['list'] = new \stdClass();
		}

		//      foreach($this->attributes['list'] as $dictionaryId => $list) {
		//      	if (is_object($list)) {
		//      		$list = get_object_vars($list);
		//      	}
		//      	if (is_array($list)) {
		//      		$list = '||'.implode('||', $list).'||';
		//      	}
		// $this->attributes['list']->$dictionaryId = $list;
		//      }

		return $this->attributes['list'];
	}

	public function save(array $options = []) {
		$hash = parent::save($options);
		Storage::disk('shared')->put('userList', $hash);
		return $hash;
	}

}

/*
stadja1@mailinator.com
- Public Address: GCVIB4DWVIUG4GAMPWOFSQKHDHFVM5JDW74A7QUG3MLWACJBSOMO2UAE
- Secret Key: SDRM6NBK6NGZWBDPF747DAW6VRHAMEDXNTGAOGWM5ZCXI7OK4S6QCJ3B
 */
