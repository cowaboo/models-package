<?php

namespace Cowaboo\Models\Authentication;

use Illuminate\Contracts\Auth\Authenticatable;
use Session;

class IPFSUser implements Authenticatable {
	/**
	 * @return string
	 */
	public function getAuthIdentifierName() {
		// Return the name of unique identifier for the user (e.g. "id")
		return 'email';

	}

	/**
	 * @return mixed
	 */
	public function getAuthIdentifier() {
		// Return the unique identifier for the user (e.g. their ID, 123)
		return $this->email;
	}

	/**
	 * @return string
	 */
	public function getAuthPassword() {
		// Returns the (hashed) password for the user
		return false;
	}

	/**
	 * @return string
	 */
	public function getRememberToken() {
		// Return the token used for the "remember me" functionality
		Session::get('rememberToken');
	}

	/**
	 * @param  string  $value
	 * @return void
	 */
	public function setRememberToken($value) {
		// Store a new token user for the "remember me" functionality
		Session::put('rememberToken', $value);
	}

	/**
	 * @return string
	 */
	public function getRememberTokenName() {
		// Return the name of the column / attribute used to store the "remember me" token
		return 'rememberToken';
	}
}
