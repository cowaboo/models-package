<?php
namespace Cowaboo\Models\Authentication;

use Cowaboo\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider as IlluminateUserProvider;

class IPFSUserProvider implements IlluminateUserProvider {
	/**
	 * @param  mixed  $identifier
	 * @return \Illuminate\Contracts\Auth\Authenticatable|null
	 */
	public function retrieveById($identifier) {
		// Get and return a user by their unique identifier
		$user = User::retrieveById($identifier);
		return $user;
	}

	/**
	 * @param  mixed   $identifier
	 * @param  string  $token
	 * @return \Illuminate\Contracts\Auth\Authenticatable|null
	 */
	public function retrieveByToken($identifier, $token) {
		// Get and return a user by their unique identifier and "remember me" token
		$user = User::retrieveByToken($identifier, $token);
		return $user ? $user : NULL;
	}

	/**
	 * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
	 * @param  string  $token
	 * @return void
	 */
	public function updateRememberToken(Authenticatable $user, $token) {
		// Save the given "remember me" token for the given user
		$user->setRememberToken($token);
	}

	/**
	 * Retrieve a user by the given credentials.
	 *
	 * @param  array  $credentials
	 * @return \Illuminate\Contracts\Auth\Authenticatable|null
	 */
	public function retrieveByCredentials(array $credentials) {
		if (!isset($credentials['secretKey'])) {
			return false;
		}

		$secretKey = $credentials['secretKey'];

		$user = User::retrieveByCredentials($secretKey);

		return $user;
	}

	/**
	 * Validate a user against the given credentials.
	 *
	 * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
	 * @param  array  $credentials
	 * @return bool
	 */
	public function validateCredentials(Authenticatable $user, array $credentials) {
		if (!isset($credentials['secretKey'])) {
			return false;
		}

		$secretKey = $credentials['secretKey'];

		$user = User::retrieveByCredentials($secretKey);

		return isset($user->email) && $user->email ? true : false;
	}

}
