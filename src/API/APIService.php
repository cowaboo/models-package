<?php
namespace Cowaboo\Models\API;

/**
 *
 */
class APIService {
	// protected $url = "http://stadja.net:81/rest/cowaboo/";
	protected $url = "http://prototype.stadja.net/cowaboo/api/";

	function __construct() {
	}

	public function post($method, $fields) {
		$fields['app_name'] = 'cowaboo';

		//extract data from the post
		//set POST variables
		$url = $this->url . $method;
		// $url = "http://stadja.net:81/rest/cowaboo/ipfst/raw";

		foreach ($fields as $key => $value) {
			$fields[$key] = urlencode($value);
		}

		//url-ify the data for the POST
		$fields_string = '';
		foreach ($fields as $key => $value) {
			$fields_string .= $key . '=' . $value . '&';
		}
		rtrim($fields_string, '&');

		//open connection
		$ch = curl_init();

		//set the url, number of POST vars, POST data
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, count($fields));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		//execute post
		$result = curl_exec($ch);

		if (is_string($result)) {
			$test = json_decode($result);
		}
		if (!isset($test->Hash) || !$test->Hash) {
			dd('Error when posting', $result, $url, count($fields), $fields, $fields_string);
		}

		//close connection
		curl_close($ch);

		return $result;
	}

}
