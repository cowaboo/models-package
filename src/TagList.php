<?php

namespace Cowaboo\Models;

use Storage;

class TagList extends IPFSable {
	protected $keys = array('list', 'previous', 'date');
	protected $mainKey = 'tag_list';

	static function getCurrent() {
		$current = false;
		if (Storage::disk('shared')->has('tagList')) {
			$hash = Storage::disk('shared')->get('tagList');
			if ($hash) {
				$current = self::createFromHash($hash);
			}
		}
		if (!$current) {
			$current = new self();
			$current->save();
		}
		return $current;
	}

	public function get() {
		return self::getCurrent();
	}

	public function saveDictionary($dictionary) {
		$id = $dictionary->id;
		$newTagList = $this->createNewVersion();
		$list = clone ($newTagList->list);
		$list->$id = $dictionary->tagList;
		$newTagList->list = $list;

		if ($newTagList->list == $this->list) {
			return $this->hash;
		}
		return $newTagList->save();
	}

	public function removeDictionaryId($dictionaryId) {
		$newTagList = $this->createNewVersion();
		$list = clone ($newTagList->list);
		unset($list->$dictionaryId);
		$newTagList->list = $list;
		return $newTagList->save();
	}

	public function getListAttribute() {
		if (!isset($this->attributes['list']) || !$this->attributes['list']) {
			$this->attributes['list'] = new \stdClass();
		}

		foreach ($this->attributes['list'] as $dictionaryId => $list) {
			if (is_object($list)) {
				$list = get_object_vars($list);
			}
			if (is_array($list)) {
				$list = '||' . implode('||', $list) . '||';
			}
			$this->attributes['list']->$dictionaryId = $list;
		}

		return $this->attributes['list'];
	}

	public function save(array $options = []) {
		$hash = parent::save($options);
		Storage::disk('shared')->put('tagList', $hash);
		return $hash;
	}

}
