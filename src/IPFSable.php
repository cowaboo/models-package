<?php

namespace Cowaboo\Models;

use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use IPFS;

class IPFSAble extends Model {
	/**
	 * @var string
	 */
	public $hash = '';

	/**
	 * @var mixed
	 */
	public $incrementing = false;

	/**
	 * @var array
	 */
	protected $keys = array();

	/**
	 * @var mixed
	 */
	protected $locked = false;

	/**
	 * @var string
	 */
	protected $mainKey = '';

	/**
	 * @var mixed
	 */
	protected $serializable;

	public function __construct() {
		parent::__construct();
	}

	public function __toString() {
		return json_encode($this);
	}

	/**
	 * @param $confName
	 * @param $confValue
	 */
	public function addConf($confName, $confValue) {
		$conf = $this->conf;
		if (!is_array($conf)) {
			$conf = get_object_vars($conf);
		}
		$json = json_decode($confValue);
		$confValue = $json ? $json : $confValue;
		$conf[$confName] = $confValue;
		$this->conf = $conf;
	}

	/**
	 * @param  $hash
	 * @return mixed
	 */
	public static function createFromHash($hash) {
		$ipfs = IPFS::get($hash);
		$type = IPFS::findType($ipfs);

		if ((new static )->mainKey != $type) {
			return $ipfs;
		}

		$newObject = self::createFromIpfs($ipfs);
		$newObject->hash = $hash;
		return $newObject;

	}

	/**
	 * @param  $ipfs
	 * @return mixed
	 */
	public static function createFromIpfs($ipfs) {
		$object = new static;
		$mainKey = $object->mainKey;

		if ($ipfs) {
			$object->serializable = $ipfs;

			foreach ($object->keys as $key) {
				if (isset($ipfs->$mainKey->$key)) {
					$object->$key = $ipfs->$mainKey->$key;
				}
			}

			$object->locked = true;
		}
		return $object;
	}

	/**
	 * @return mixed
	 */
	public function createNewVersion() {
		$new = $this->replicate(array('serializable', 'locked', 'date'));
		$new->previous = $this->hash;
		return $new;
	}

	public function getAuthorAttribute() {
		return isset($this->attributes['author']) && $this->attributes['author'] ? $this->attributes['author'] : Auth::user()->email;
	}

	/**
	 * @param  $confId
	 * @return mixed
	 */
	public function getConf($confId) {
		$conf = $this->conf;
		if (!is_array($conf)) {
			$conf = get_object_vars($conf);
		}
		if (!isset($conf[$confId])) {
			return false;
		}

		return $conf[$confId];
	}

	/**
	 * @return mixed
	 */
	public function getConfAttribute() {
		if (!isset($this->attributes['conf']) || !$this->attributes['conf']) {
			$this->attributes['conf'] = array();
		}
		return $this->attributes['conf'];
	}

	/**
	 * @return mixed
	 */
	public function getDateAttribute() {
		if (!$this->locked) {
			$this->attributes['date'] = Carbon::now()->format('YmdHi');
		} else {
			$date = isset($this->attributes['date']) ? $this->attributes['date'] : false;
			if ('0000-00-00' == $date) {
				$date = false;
			}
			$date = $date ? Carbon::createFromFormat('YmdHi', $date) : '';
			return $date ? $date->format('d/m/Y H:i') : false;
		}
		return $this->attributes['date'];
	}

	/**
	 * @param $format
	 */
	public function getDateWithFormat($format) {
		$date = isset($this->attributes['date']) ? $this->attributes['date'] : false;

		if (!$date) {
			$date = Carbon::now();
		}
		return Carbon::createFromFormat('YmdHi', $date)->format($format);
	}

	public function getListAttribute() {
		return isset($this->attributes['list']) && $this->attributes['list'] ? $this->attributes['list'] : array();
	}

	/**
	 * @return mixed
	 */
	public function getPreviousObjectAttribute() {
		if (!$this->previous) {
			return false;
		}
		$object = self::createFromHash($this->previous);
		return $object;
	}

	/**
	 * Convert the object into something JSON serializable.
	 *
	 * @return array
	 */
	public function jsonSerialize() {
		if (!$this->locked) {
			$this->serializable = new \stdClass();
			$mainKey = $this->mainKey;
			$this->serializable->$mainKey = new \stdClass();

			foreach ($this->keys as $key) {
				$this->serializable->$mainKey->$key = $this->$key;
			}

			$this->locked = true;
		}
		return $this->serializable;
	}

	/**
	 * @param  array   $options
	 * @return mixed
	 */
	public function save(array $options = []) {
		$hash = IPFS::create($this);
		$this->hash = $hash;
		return $hash;
	}
}
