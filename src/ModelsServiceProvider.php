<?php

namespace Cowaboo\Models;
use Auth;
use Cowaboo\Models\Authentication\IPFSUserProvider;
use Illuminate\Support\ServiceProvider;

class ModelsServiceProvider extends ServiceProvider {
	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot() {
		Auth::provider('ipfs_provider', function ($app, array $config) {
			return new IPFSUserProvider();
		});

		$config = $this->app['config']->get('filesystems', []);
		$config['disks'] = array_merge(require __DIR__ . '/config/disks.php', $config['disks']);
		$this->app['config']->set('filesystems', $config);

		$config = $this->app['config']->get('auth', []);
		$config['providers']['users'] = [
			'driver' => 'ipfs_provider',
		];
		$this->app['config']->set('auth', $config);

	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register() {
		$this->app->singleton('IPFS', function ($app) {
			return new \Cowaboo\Models\IPFS\IPFSService();
		});
		$this->app->singleton('API', function ($app) {
			return new \Cowaboo\Models\API\APIService();
		});
		$this->app->singleton('Stellar', function ($app) {
			return new \Cowaboo\Models\Stellar\StellarService();
		});
		$this->app->singleton('TagList', function ($app) {
			return \Cowaboo\Models\TagList::getCurrent();
		});
		$this->app->singleton('UserList', function ($app) {
			return \Cowaboo\Models\UserList::getCurrent();
		});
		$loader = \Illuminate\Foundation\AliasLoader::getInstance();

		$loader->alias('IPFS', \Cowaboo\Models\IPFS\IPFSFacade::class);
		$loader->alias('API', \Cowaboo\Models\API\APIFacade::class);
		$loader->alias('Stellar', \Cowaboo\Models\Stellar\StellarFacade::class);
		$loader->alias('UserList', \Cowaboo\Models\UserList\UserListFacade::class);
		$loader->alias('TagList', \Cowaboo\Models\TagList\TagListFacade::class);
	}

	public function provides() {
		return array('IPFS', 'Stellar', 'TagList', 'UserList', 'API');
	}
}
