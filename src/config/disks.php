<?php

return [
	'ipfs' => [
		'driver' => 'local',
		'root' => storage_path('ipfs'),
	],

	'dictionaries' => [
		'driver' => 'local',
		'root' => storage_path('dictionaries'),
	],

	'shared' => [
		'driver' => 'local',
		'root' => storage_path('shared'),
	],

	'propositions' => [
		'driver' => 'local',
		'root' => storage_path('propositions'),
	],
];
