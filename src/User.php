<?php

namespace Cowaboo\Models;

// use Illuminate\Foundation\Auth\User as Authenticatable;
use Cowaboo\Models\Dictionary;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Session;
use Stellar;
use TagList;
use UserList as Users;

// class User extends Authenticatable
class User extends Model implements Authenticatable {
	/**
	 * @var mixed
	 */
	protected $otherDictionaries = false;

	/**
	 * @var mixed
	 */
	protected $yourDictionaries = false;

	/**
	 * @param $email
	 * @param false    $publicAddress
	 */
	public function __construct($email = false, $publicAddress = false) {
		// parent::__construct();
		$this->email         = $email;
		$this->publicAddress = $publicAddress;
	}

	/**
	 * @return mixed
	 */
	public function getAuthIdentifier() {
		// Return the unique identifier for the user (e.g. their ID, 123)
		return $this->email;
	}

	/**
	 * @return string
	 */
	public function getAuthIdentifierName() {
		// Return the name of unique identifier for the user (e.g. "id")
		return 'email';

	}

	/**
	 * @return string
	 */
	public function getAuthPassword() {
		// Returns the (hashed) password for the user
		return false;
	}

	/**
	 * @return mixed
	 */
	public function getDictionariesAttribute() {
		if (!$this->yourDictionaries) {

			$this->yourDictionaries = array();
			$dictionaries           = TagList::get()->list;

			foreach (TagList::get()->list as $dictionaryId => $tags) {
				$dictionary = Dictionary::getCurrentFromId($dictionaryId);
				if (!is_array($dictionary->member_list)) {
					$dictionary->member_list = (array) $dictionary->member_list;
				}
				if (in_array($this->email, $dictionary->member_list)) {
					$this->yourDictionaries[$dictionaryId] = $tags;
				}
			}
		}

		return $this->yourDictionaries;
	}

	/**
	 * @return mixed
	 */
	public function getOtherDictionariesAttribute() {
		if (!$this->otherDictionaries) {
			$this->otherDictionaries = array();
			$yourDictionaries        = $this->dictionaries;

			foreach (TagList::get()->list as $dictionaryId => $tags) {
				$dictionary = Dictionary::getCurrentFromId($dictionaryId);
				if (!isset($yourDictionaries[$dictionaryId]) && !$dictionary->getConf('private')) {
					$this->otherDictionaries[$dictionaryId] = $tags;
				}
			}
		}
		return $this->otherDictionaries;
	}

	/**
	 * @return string
	 */
	public function getRememberToken() {
		// Return the token used for the "remember me" functionality
		$token = isset($_SESSION['rememberToken']) ? $_SESSION['rememberToken'] : false;
		return $token;
	}

	/**
	 * @return string
	 */
	public function getRememberTokenName() {
		// Return the name of the column / attribute used to store the "remember me" token
		return 'rememberToken';
	}

	/**
	 * @return mixed
	 */
	public function jsonSerialize() {
		$serializable                              = new \stdClass();
		$serializable->email                       = $this->email;
		$serializable->publicAddress               = $this->publicAddress;
		$serializable->observatories               = new \stdClass();
		$serializable->observatories->subscribed   = $this->dictionaries;
		$serializable->observatories->unsubscribed = $this->other_dictionaries;
		return $serializable;
	}

	/**
	 * @param $secretKey
	 */
	public static function retrieveByCredentials($secretKey) {
		$keyPair = Stellar::getKeyPairBySecretKey($secretKey);
		if (!isset($keyPair->public_address)) {
			return new self();
		}
		$emailFound = Users::findEmailByPublicAddress($keyPair->public_address);
		return new self($emailFound, $keyPair->public_address);
	}

	/**
	 * @param  $email
	 * @return mixed
	 */
	public static function retrieveById($email) {
		$publicAddress = Users::findUser($email);
		if (!$publicAddress) {
			return false;
		}
		$user = new self($email, $publicAddress);
		return $user;
	}

	/**
	 * @param  $email
	 * @param  $token
	 * @return mixed
	 */
	public static function retrieveByToken($email, $token) {
		$publicAddress = Users::findUser($email);
		$tokenComputed = md5($publicAddress . $token);
		if ($tokenComputed != $token) {
			return false;
		}
		$user = new self($email, $publicAddress);
		return $user;
	}

	/**
	 * @param  string $value
	 * @return void
	 */
	public function setRememberToken($token) {
		$tokenComputed             = md5($this->email . $token);
		$_SESSION['rememberToken'] = $tokenComputed;
		Session::put('rememberToken', $tokenComputed);
	}
}
