<?php
namespace Cowaboo\Models\Stellar;

/**
 *
 */
class StellarService {
	protected $url   = "http://stadja.net:8081";
	protected $cache = array();

	function __construct() {
	}

	public function getAccountBalance($public) {
		$balances = $this->get('/account/balances', compact('public'));
		$balances = json_decode($balances);
		if (isset($balances->error)) {
			return false;
		}
		if (!$balances->result || !$balances->result->value || !$balances->result->value->energy || !$balances->result->value->energy->balance) {
			return 0;
		}
		$balance = (int) $balances->result->value->energy->balance - 10000;
		return $balance;
	}

	public function getPaymentAccount($public, $secret, $destination, $amount) {
		$payment = $this->get('/account/payment', compact('public', 'secret', 'destination', 'amount'));
		$payment = json_decode($payment);
		if (isset($payment->error)) {
			return $payment;
		}
		return true;
	}

	public function getInitAccount($public, $secret) {
		$fund = $this->get('/account/init', compact('public', 'secret'));
		$fund = json_decode($fund);
		if (isset($fund->error)) {
			return false;
		}
		return true;
	}

	public function getKeyPairBySecretKey($secretKey) {
		if (!isset($this->cache[$secretKey])) {
			$keyPair                 = $this->get('/keyPair/fromSecret', array('secret' => $secretKey));
			$this->cache[$secretKey] = json_decode($keyPair);
		}
		return $this->cache[$secretKey];
	}

	public function getNewKeyPair() {
		$keyPair = new \stdClass();
		$keyPair = $this->get('/keyPair/new');
		$keyPair = json_decode($keyPair);
		return $keyPair;
	}

	public function get($method, $fields = array()) {

		//extract data from the post
		//set POST variables
		$url = $this->url . $method;

		foreach ($fields as $key => $value) {
			$fields[$key] = urlencode($value);
		}

		//url-ify the data for the POST
		$fields_string = '';
		foreach ($fields as $key => $value) {
			$fields_string .= $key . '=' . $value . '&';
		}
		rtrim($fields_string, '&');

		$url .= $fields_string ? '?' . $fields_string : '';
		//open connection
		$ch = curl_init();

		//set the url, number of POST vars, POST data
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		//execute post
		$result = curl_exec($ch);

		if (is_string($result)) {
			$test = json_decode($result);
		}

		//close connection
		curl_close($ch);

		return $result;
	}

	public function post($method, $fields) {
		//extract data from the post
		//set POST variables
		$url = $this->url . $method;

		foreach ($fields as $key => $value) {
			$fields[$key] = urlencode($value);
		}

		//url-ify the data for the POST
		$fields_string = '';
		foreach ($fields as $key => $value) {
			$fields_string .= $key . '=' . $value . '&';
		}
		rtrim($fields_string, '&');

		//open connection
		$ch = curl_init();

		//set the url, number of POST vars, POST data
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, count($fields));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		//execute post
		$result = curl_exec($ch);

		if (is_string($result)) {
			$test = json_decode($result);
		}
		if (!isset($test->Hash) || !$test->Hash) {
			dd($result, $url, $fields, $fields_string);
		}

		//close connection
		curl_close($ch);

		return $result;
	}
}
