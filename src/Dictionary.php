<?php

namespace Cowaboo\Models;

use Auth;
use Cowaboo\Models\Entry;
use Cowaboo\Models\IPFSable;
use Storage;
use TagList as TagListDictionary;

class Dictionary extends IPFSable {
	/**
	 * @var array
	 */
	protected $keys = array('id', 'entries', 'member_list', 'date', 'conf', 'previous', 'author', 'metadata');

	/**
	 * @var string
	 */
	protected $mainKey = 'dictionary';

	/**
	 * @param $entry
	 */
	public function addEntry($entry) {
		$tags = $entry->tags;
		$entries = $this->entries;
		$entries->$tags = $entry->hash;
		$this->entries = $entries;
		return true;
	}

	/**
	 * @param $email
	 */
	public function addUser($email) {
		$this->member_list = array_unique(array_merge($this->member_list, array($email)));
		return true;
	}

	/**
	 * @param  $id
	 * @return mixed
	 */
	public static function createNew($id) {
		$list = TagListDictionary::getCurrent();

		foreach ($list->list as $dicoId => $tags) {
			if (strtolower($id) == strtolower($dicoId)) {
				return false;
			}
		}

		$dictionary = new self();
		$dictionary->id = $id;
		$dictionary->addUser(Auth::user()->email);
		$hash = $dictionary->save();
		return $hash;
	}

	/**
	 * @return mixed
	 */
	public function createNewVersion() {
		$new = parent::createNewVersion();
		$new->id = $this->id;
		return $new;
	}

	public function currentUserIsMember() {
		$current = Auth::user();
		foreach ($this->memberList as $email) {
			if (strtolower($email) == strtolower($current->email)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @param  $tag
	 * @return mixed
	 */
	public function findEntry($tag) {
		$entries = $this->entry_objects;
		$english = false;
		$transl = false;
		$normal = false;

		if (isset($this->entry_objects[$tag])) {
			$normal = $this->entry_objects[$tag];
		}

		if (isset($this->entry_objects['_en,' . $tag])) {
			$english = $this->entry_objects['_en,' . $tag];
		}

		$lang = isset($_COOKIE['lang']) && $_COOKIE['lang'] ? $_COOKIE['lang'] : substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);

		if ($lang && isset($this->entry_objects['_' . $lang . ',' . $tag])) {
			$transl = $this->entry_objects['_' . $lang . ',' . $tag];
		}

		if ($transl) {
			return $transl;
		} elseif ($english) {
			return $english;
		}

		return $normal;
	}

	/**
	 * @param  $entry
	 * @return mixed
	 */
	public function getCurrentEntryHash($entry) {
		$tags = $entry->tags;
		if (!isset($this->entries->$tags)) {
			return false;
		}
		return $this->entries->$tags;
	}

	/**
	 * @param  $id
	 * @return mixed
	 */
	public static function getCurrentFromId($id) {
		if (!Storage::disk('dictionaries')->exists($id)) {
			return false;
		}

		$hash = Storage::disk('dictionaries')->get($id);
		$dictionary = self::createFromHash($hash);
		return $dictionary;
	}

	/**
	 * @return mixed
	 */
	public function getEntriesAttribute() {
		if (!isset($this->attributes['entries']) || !$this->attributes['entries']) {
			$this->attributes['entries'] = new \stdClass();
		}
		$entries = $this->attributes['entries'];
		return $entries;
	}

	/**
	 * @return mixed
	 */
	public function getEntriesReverseAttribute() {
		if (!isset($this->attributes['entries']) || !$this->attributes['entries']) {
			$this->attributes['entries'] = new \stdClass();
		}
		$entries = (object) array_reverse(get_object_vars($this->attributes['entries']));
		return $entries;
	}

	/**
	 * @return mixed
	 */
	public function getEntryObjectsAttribute() {
		$entries = $this->entries;
		$entries = get_object_vars($entries);
		$entriesObjects = array();
		foreach ($entries as $tags => $entryHash) {
			$tags = array_filter(explode('||', $tags));
			$tags = implode(',', $tags);
			$entriesObjects[$tags] = Entry::createFromHash($entryHash);
		}
		return $entriesObjects;
	}

	/**
	 * @return mixed
	 */
	public function getMemberListAttribute() {
		if (!isset($this->attributes['member_list']) || !$this->attributes['member_list']) {
			$this->attributes['member_list'] = array();
		}
		return $this->attributes['member_list'];
	}

	/**
	 * @return mixed
	 */
	public function getTagListAttribute() {
		$tagList = array();
		foreach ($this->entries as $tags => $entry) {
			$tags = explode('||', $tags);
			$tagList = array_filter(array_unique(array_merge($tags, $tagList)));
			sort($tagList);
		}
		return $tagList;
	}

	/**
	 * @param $entry
	 */
	public function isCurrentEntry($entry) {
		return in_array($entry->hash, get_object_vars($this->entries));
	}

	/**
	 * @param $entry
	 */
	public function removeEntry($entry) {
		$tags = $entry->tags;
		$entries = $this->entries;
		unset($entries->$tags);
		$this->entries = $entries;
		return true;
	}

	/**
	 * @param  array   $options
	 * @return mixed
	 */
	public function save(array $options = []) {
		$hash = parent::save($options);
		Storage::disk('dictionaries')->put($this->id, $hash);
		$tagList = TagListDictionary::getCurrent($this);
		$tagList->saveDictionary($this);
		return $hash;
	}

	/**
	 * @param  $tags
	 * @return mixed
	 */
	public function setTagsAttribute($tags) {
		$this->attributes['tags'] = array_filter(explode('||', trim($tags)));
		sort($this->attributes['tags']);
		return $this->attributes;
	}
}
